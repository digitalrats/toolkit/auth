package org.digitalrats.toolkit.auth;

public interface AuthService {
    /**
     * Аутентификация предоставленого токена.
     *
     */
    String authenticate(String token);
}
