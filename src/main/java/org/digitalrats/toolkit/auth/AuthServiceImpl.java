package org.digitalrats.toolkit.auth;

import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.VaultException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;

import javax.crypto.SecretKey;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Key;
import java.time.Instant;
import java.util.UUID;

public class AuthServiceImpl implements Service, AuthService {
    /** Auto-injected instance of Ignite. */
    @IgniteInstanceResource
    private Ignite ignite;

    private String vaultToken;
    private String svcName;
    private IgniteCache<UUID, String> cache;


    /** \todo Брать это из Vault */

    private final String issuer = "https://scaffold.me";

    private String getKey(URL vaultURL, String vaultToken, String name) {
        try {
            final VaultConfig config = new VaultConfig()
                    .address(vaultURL.toString())
                    .token(vaultToken)
                    .build();
            final Vault vault = new Vault(config);
            return vault.logical()
                    .read("secret/toolkit")
                    .getData().get(name);
        } catch (VaultException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void cancel(ServiceContext serviceContext) {

    }

    public void init(ServiceContext serviceContext) {
        // Предватрительно настроенный кэш для хранения ролей.
        vaultToken = "some vault token";
        cache = ignite.cache("session");
        svcName = serviceContext.name();
    }

    public void execute(ServiceContext serviceContext) {
        // Тут ничего не нужно делать.
    }

    public String authenticate(String token) {
        // Тут нужно расшифровть полученный токен и определить максимальную роль
        // предоставившего токен.
        // Примечание: в дальнейшем возможен переход к более сложной системе разграничения доступа.
        final String key;
        try {
            key = getKey(new URL("http://127.0.0.1:8200"), vaultToken, "auth");
            SecretKey secretKey;
            if (key != null) {
                secretKey = Keys.hmacShaKeyFor(key.getBytes());
            } else {
                return null;
            }
            Jws<Claims> claims = Jwts.parserBuilder()
                    .requireIssuer(issuer)
                    .setSigningKey(secretKey)
                    .build()
                    .parseClaimsJws(token);
            // \todo Реализовать получение роли по идентификатору сессии
            UUID uuid = UUID.fromString(claims.getBody().getSubject().replace("auth|",""));
            return "probe";
        } catch (MalformedURLException | MissingClaimException | IncorrectClaimException | ExpiredJwtException e) {
            e.printStackTrace();
            return null;
        }

    }
}
