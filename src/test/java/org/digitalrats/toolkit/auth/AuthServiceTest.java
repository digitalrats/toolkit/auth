package org.digitalrats.toolkit.auth;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.ignite.configuration.AtomicConfiguration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public class AuthServiceTest {
    @ParameterizedTest(name = "authenticate(\"{0}\")")
    @ValueSource(strings = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3NjYWZmb2xkLm1lIiwic3ViIjoiYXV0aHwwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJleHAiOjE3NDUzNDAyNTksImlhdCI6MTU4NzU1NTQ5OX0.m0s4MQz7mbTgUGWtcleE00orNdRK1BzG6NjHZ7ED3ps")
    void authenticateToken(String token) {
        AuthServiceImpl service = new AuthServiceImpl();
        assertEquals("user", service.authenticate(token));
    }

    /*
    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0,    1,   1",
            "1,    2,   3",
            "49,  51, 100",
            "1,  100, 101"
    })
    void add(int first, int second, int expectedResult) {
        Calculator calculator = new Calculator();
        assertEquals(expectedResult, calculator.add(first, second),
                () -> first + " + " + second + " should equal " + expectedResult);
    }
     */
}
